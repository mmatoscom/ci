FROM ruby:2.4.0-alpine
ADD ./ /app/
WORKDIR /app
ENV PORT 80
EXPOSE 80

CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]