require 'webrick'

server = WEBrick::HTTPServer.new :Port => 80

server.mount_proc '/' do |req, res|
  res.body = 'Hello, world!'
end

server.start